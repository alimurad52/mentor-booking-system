const Slots = require('../models/slots.model');
const axios = require('axios');
const moment = require('moment');
const momentUtc = (date) => moment().utc(date);

//create slots
exports.create = (req, res) => {
    const slots = new Slots({
        startTime: req.body.startTime,
        endTime: req.body.endTime,
        reason: req.body.reason
    });
    //save slot to mongodb
    slots.save()
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Failed to create slot. Try again."
            });
        });
};

//get all slots
exports.findAll = async (req, res) => {
    getSlotsFromCF()
        .then((cfRes) => {
            Slots.find()
                .then(slots => {
                    let resToReturn = constructObj(cfRes, slots);
                    res.send(resToReturn)
                })
                .catch(err => {
                    res.status(500).send({
                        message: err.message || "Error occurred while getting data. Try again."
                    });
                });
        })
        .catch((err) => {
            res.status(500).send({
                message: err.message || "Something went wrong. Try again."
            });
        });

};

//retrieve slots from CF
function getSlotsFromCF() {
    return new Promise((resolve,reject) => {
        axios.get('https://private-37dacc-cfcalendar.apiary-mock.com/mentors/1/agenda')
            .then((res) => {
                resolve(res.data);
            })
            .catch((err) => {
                reject(err);
            })

    })
}

//combine the response from cf api and local api call to get all slots in one call
function constructObj(cfRes, localRes) {
    if(cfRes) {
        let allocatedSlots = [];
        //format cf response to local response
        for(let i = 0; i < cfRes.calendar.length; i++) {
            let date = cfRes.calendar[i].date_time;
            let tempObj = {
                startTime: moment(date),
                endTime: null,
                reason: ''
            };
            tempObj.endTime = moment(tempObj.startTime).add(1, 'hours').format();
            allocatedSlots.push(tempObj);
        }
        //checking if local res slots exists in cf response slots
        for(let i = 0; i < localRes.length; i++) {
            let date = localRes[i];
            //to ensure the object is only pushed once
            //the count would only increase when there's a conflict between allocated slots
            let count = 0;
            for(let i = 0; i < allocatedSlots.length; i++) {
                let eDate = allocatedSlots[i];
                if(moment(eDate.startTime).isSame(date.startTime)) {
                    count++;
                }
            }
            if(count === 0) allocatedSlots.push({
                startTime: date.startTime,
                endTime: date.endTime,
                reason: date.reason
            });
        }
        return {
            mentor: cfRes.mentor,
            slots: allocatedSlots
        };
    }
    return localRes;
}
