module.exports = (app) => {
    const slots = require('../controllers/slots.controller');

    //routes for slots
    app.post('/slots', slots.create);
    app.get('/slots', slots.findAll);
};
