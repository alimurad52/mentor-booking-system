const mongoose = require('mongoose');
const moment = require('moment');
//create slot schema in mongodb
const SlotsScheme = mongoose.Schema({
    startTime: {
        type: String,
        required: true
    },
    endTime: {
        type: String,
        required: true
    },
    reason: {
        type: String
    },
    isDeleted: {
        type: Boolean,
        default: false,
    },
    deletedAt: {
        type: Date,
        default: null,
    },
}, {
    timestamps: { currentTime: () => moment().format() },
});

module.exports = mongoose.model('Slots', SlotsScheme);
