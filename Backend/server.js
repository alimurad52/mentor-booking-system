require("dotenv").config();
const express = require('express');
const app = express();
const cors =  require('cors');

app.use(cors());
//initialize middleware
app.use(express.json({ extended: false }));


const mongoose = require('mongoose');

mongoose.connect(process.env.URL, {
    useNewUrlParser: true
}).then(() => {
    console.log("Successfully connected to the database");
}).catch(err => {
    console.log('Could not connect to db.', err);
    process.exit();
});

require('./app/routes/slots.routes')(app);

app.listen(process.env.PORT, () => {
    console.log(`Server is listening on port ${process.env.PORT}`);
});
