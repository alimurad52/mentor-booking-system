### [Demo for get slots](https://careerfoundry-api.herokuapp.com/slots)

# Getting Started

- Run `git clone https://bitbucket.org/alimurad52/mentor-booking-system/src/master/`
- `cd Backend`
- Run `npm install`
- Run `node server.js`

# Available routes and methods
 
## `GET` request `/slots`
Retrieves the local mongo db data as well as fetches the data from CareerFoundry API and merges the results to return one `Array` of all allocated slots. This has two purposes:  
  1. Mask the CareerFoundry URL from the frontend   
  2. Frontend to receive only one `Array` of all slots combined. 

Includes validation check if the slot from local db is booked, since it was mentioned in the assumptions that allocated slots can change anytime, and then prioritize the slot received from CareerFoundry API.
## `POST` request `/slots`
 
 - Expected parameters
    - `startDate: String`: `required`
    - `endDate: String`: `required`
    - `reson: String`
