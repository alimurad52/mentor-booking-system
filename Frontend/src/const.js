import axios from 'axios';
import {Alert} from "@material-ui/lab";
import {Snackbar} from "@material-ui/core";
import React from "react";

const baseUrl = 'https://careerfoundry-api.herokuapp.com/slots';

export const getSchedule = () => {
    return new Promise((resolve, reject) => {
        axios.get(baseUrl)
            .then(res => {
                if (res.status === 200) {
                    resolve(res.data);
                }
            }).catch(err => reject(err));
    })
};

export const bookSchedule = (startDate, endDate, reason) => {
    return new Promise((resolve, reject) => {
        let data = {
            startTime: startDate,
            endTime: endDate,
            reason: reason
        };
        axios.request({
            method: 'POST',
            url: baseUrl,
            data: data,
            headers: {'Content-Type': 'application/json'}
        }).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        })
    })
};

export const AlertBar = ({type, show, set, message}) => (
    <Snackbar open={show} autoHideDuration={6000} onClose={() => set(false)}>
        <Alert severity={type}>{message}</Alert>
    </Snackbar>
);
