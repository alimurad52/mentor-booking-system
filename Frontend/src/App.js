import React, {useEffect, useState} from 'react';
import ReactCalendar from "react-calendar";
import 'react-calendar/dist/Calendar.css';
import {getSchedule, bookSchedule, AlertBar} from "./const";
import './_styles.scss';
import moment from "moment";

import Sidebar from "./components/Sidebar";
import Topbar from "./components/Topbar";

export default function App() {
    const [drawer, setDrawer] = useState(false);
    const [date, setDate] = useState(new Date());
    const [data, setData] = useState(null);
    const [mentor, setMentor] = useState('');
    const [isLoading, setLoading] = useState(false);
    const [showSuccess, setSuccess] = useState(false);

    const onChangeDrawer = (date) => {
        setDate(date);
        setDrawer(true);
        setData(null);
        getSchedule().then(res => setData(res)).catch(err => console.log(err));
    };

    useEffect(() => {
        //check if mentor isn't there retrieve mentor data
        if(!mentor) {
            getSchedule().then(res => setMentor(res.mentor.name)).catch(err => console.log(err));
        }
    }, [data]);

    const bookSlot = (hour, date, reason) => {
        setLoading(true);
        //post request to create a new slot
        return new Promise((resolve, reject) => {
            let dateFormatted = moment(date).format('YYYY-MM-DD');
            let formattedTime = moment.utc(hour*3600*1000).format('HH:mm');
            let startDate = moment(`${dateFormatted} ${formattedTime}`).format();
            let endDate = moment(`${dateFormatted} ${formattedTime}`).add(1, 'hour').format();
            bookSchedule(startDate, endDate, reason).then(res => {
                setLoading(false);
                if(res.status === 200) {
                    //retrieve data after pushing new slot
                    getSchedule().then(res => setData(res)).catch(err => console.log(err));
                    setSuccess(true);
                    resolve(res);
                } else {
                    reject(res);
                }
            }).catch(err => {
                setLoading(false);
                reject(err);
            })

        })
    };

    return (
        <React.Fragment>
            <Topbar
                mentor={mentor}
            />
            <ReactCalendar
                onChange={onChangeDrawer}
                drawer={drawer}
                value={new Date()}
            />
            <Sidebar
                onChangeDrawer={setDrawer}
                drawer={drawer}
                date={date}
                data={data}
                bookSlot={bookSlot}
                isLoading={isLoading}
            />
            <AlertBar
                show={showSuccess}
                set={setSuccess}
                message='Slot allocated successfully'
                type="success"
            />
        </React.Fragment>
    );
};
