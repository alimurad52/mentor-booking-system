import React from "react";
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });
import Sidebar, {getHours} from './Sidebar';

describe("Sidebar component", () => {
    it("Should return expected hours object", () => {
        let tempArr = [{
            endTime: "2021-02-21T02:00:00Z",
            reason: "test",
            startTime: "2021-02-21T01:00:00Z",
        }];
        expect(getHours(tempArr)).toEqual(
            expect.arrayContaining([
                expect.objectContaining({
                    startHour: 9,
                    endHour: 10,
                    reason: 'test'
                })
            ])
        );
    });

    it("Should render Sidebar", () => {
        const wrapper = shallow(
            <Sidebar
                onChangeDrawer={() => void 0}
                drawer={true}
                date={new Date()}
                data={{slots: [{"startTime":"2029-08-05T20:24:32.000Z","endTime":"2029-08-05T21:24:32Z","reason":""}]}}
                bookSlot={() => void 0}
                isLoading={false}
            />);
    });
});
