import React, {useEffect, useState} from 'react';
import {ChevronRight} from '@material-ui/icons';
import {Drawer, List, Divider, IconButton, makeStyles, Typography, CircularProgress} from '@material-ui/core';
import Hours from "./Hours";
import moment from "moment";
import ReasonDialog from "./ReasonDialog";
import {AlertBar} from "../const";

//get the hours from the days slots
export const getHours = (daysSlots) => {
    return daysSlots.map(date => ({startHour: moment(date.startTime).get('hours'), endHour: moment(date.endTime).get('hours'), reason: date.reason}));
};

export default function TemporaryDrawer({drawer, onChangeDrawer, date, data, bookSlot, isLoading}) {
    const classes = useStyles();
    const [dateSlots, setDateSlots] = useState([]);
    const [hours, setHours] = useState([]);
    const [openDialog, setDialog] = useState(false);
    const [hourToBook, setHourToBook] = useState(null);
    const [showError, setError] = useState(false);
    const [isFetching, setFetching] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');

    //filters the slots for the selected date
    const getDateSlots = () => {
        return data.slots.filter(item => moment(item.startTime).format('YYYY-MM-DD') === moment(date).format('YYYY-MM-DD') || moment(item.endTime).format('YYYY-MM-DD') === moment(date).format('YYYY-MM-DD'));
    };

    //checks if the slot is booked or if its in the paste
    //if not shows the dialog to enter reason and book
    const onClickBook = (isBooked, hour, isPastDate) => {
        if (!isBooked && !isPastDate) {
            setHourToBook(hour);
            setDialog(true);
        } else if(isBooked) {
            setErrorMessage('Slot is already allocated');
            setError(true);
        } else {
            setErrorMessage('Selected date is past today date');
            setError(true);
        }
    };

    //set is loading state for sidebar when retrieving data
    //to show circular loading
    useEffect(() => {
        if (!data) {
            setFetching(true);
        }
        if (data) {
            console.log(getDateSlots());
            setDateSlots(getDateSlots());
            setHours(getHours(getDateSlots()));
            setFetching(false);
        }
    }, [data]);

    return (
        <div>
            <React.Fragment>
                <Drawer
                    className={classes.drawer}
                    variant="persistent"
                    anchor="right"
                    open={drawer}
                    classes={{
                        paper: classes.drawerPaper,
                    }}
                >
                    <div className={classes.drawerHeader}>
                        <IconButton
                            onClick={() => onChangeDrawer(!drawer)}
                        >
                            <ChevronRight/>
                        </IconButton>
                        <Typography
                            component={'h3'}
                        >
                            {moment(date).format('LL')}
                        </Typography>
                    </div>
                    <Divider/>
                    {
                        isFetching ?
                            <div className="loading-wrapper">
                                <CircularProgress/>
                            </div> :
                            <List className={classes.hourList}>
                                {
                                    Array(24).fill().map((_, i) => (
                                        <Hours date={date} key={i} hour={i} slots={hours} onClickBook={onClickBook}/>
                                    ))
                                }
                            </List>
                    }
                </Drawer>
            </React.Fragment>
            <ReasonDialog
                open={openDialog}
                hourToBook={hourToBook}
                date={date}
                handleClose={() => setDialog(false)}
                bookSlot={bookSlot}
                isLoading={isLoading}
            />
            <AlertBar
                show={showError}
                set={setError}
                message={errorMessage}
                type="error"
            />
        </div>
    );
};

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
    },
    drawerPaper: {
        width: drawerWidth,
        overflowX: 'hidden'
    },
    drawerHeader: {
        display: 'flex',
        alignItems: 'center',
        position: 'sticky',
        top: 0,
        background: 'white',
        zIndex: 1,
        padding: theme.spacing(0, 1),
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
        justifyContent: 'flex-start',
    },
    hourList: {
        paddingBottom: 0
    }
}));
