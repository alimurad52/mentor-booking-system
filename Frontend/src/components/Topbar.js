import React from "react";
import {AppBar, Toolbar, makeStyles, Typography} from "@material-ui/core";

export default function Topbar({mentor}) {
    const classes = useStyles();
    return (
        <AppBar position="static">
            <Toolbar className={classes.toolbar} >
                <Typography variant="h6" className={classes.title}>
                    Book an appointment with {mentor}
                </Typography>
                <small className={classes.small}>Select a date to book an appointment</small>
            </Toolbar>
        </AppBar>
    )
}

const useStyles = makeStyles((theme) => ({
    title: {
        flexGrow: 1,
    },
    toolbar: {
        display: 'block',
        paddingTop: 5,
        background: '#23275D'
    },
    small: {
        fontWeight: 100
    }
}));
