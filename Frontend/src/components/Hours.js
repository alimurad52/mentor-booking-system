import React from "react";
import {IconButton, Divider, Typography, ListItem, Chip} from "@material-ui/core";
import {Add} from "@material-ui/icons";
import moment from "moment";

export default function Hours({hour, slots, onClickBook, date}) {

    //checks if the slot is already taken
    const isBooked = (hour) => {
        if (slots.length > 0) {
            for (let i = 0; i < slots.length; i++) {
                let slot = slots[i];
                if (slot.startHour === hour || (hour > slot.startHour && hour < slot.endHour)) {
                    return true;
                }
            }
        }
        return false;
    };

    //checks if the date is past today's date
    const isPastDate = () => {
        return moment(date).isBefore(new Date(), 'day');
    };

    return (
        <ListItem>
            <div className="hour-wrapper" onClick={() => onClickBook(isBooked(hour), hour, isPastDate())}>
                <div className="hour-item">
                    {
                        isBooked(hour) && <Chip
                            variant="outlined"
                            size="small"
                            label="Slot allocated"
                            color="secondary"
                        />
                    }
                    <Typography component={'p'}>{moment.utc(hour * 3600 * 1000).format('h:mm a')}</Typography>
                    {
                        !isPastDate() && !isBooked(hour) && <IconButton><Add/></IconButton>
                    }
                </div>
                <Divider/>
            </div>
        </ListItem>
    )
}
