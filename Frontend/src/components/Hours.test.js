import React from "react";
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });
import Hours from "./Hours";
import Sidebar from "./Sidebar";

describe("Hours Component", () => {
    it("Should render 24 slots", () => {
        const wrapper = shallow(
            <Sidebar
                onChangeDrawer={() => void 0}
                drawer={true}
                date={new Date()}
                data={{slots: [{"startTime":"2029-08-05T20:24:32.000Z","endTime":"2029-08-05T21:24:32Z","reason":""}]}}
                bookSlot={() => void 0}
                isLoading={false}
            />);
        expect(wrapper.find(Hours)).toHaveLength(24);
    });
});
