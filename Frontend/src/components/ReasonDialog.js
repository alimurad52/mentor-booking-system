import React, {useState, forwardRef} from 'react';
import {
    CircularProgress,
    TextField,
    DialogTitle,
    Slide,
    DialogContent,
    DialogActions,
    Dialog,
    Button
} from '@material-ui/core'
import moment from "moment";

//adding dialog transition
const Transition = forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

export default function ReasonDialog({open, handleClose, hourToBook, date, isLoading, bookSlot}) {
    const [reason, setReason] = useState('');

    //onclick book calls the parent function
    const onClickBook = () => {
        bookSlot(hourToBook, date, reason)
            .then(res => {
                if(res.status === 200) {
                    handleClose();
                }
            })
            .catch(err => {
                console.log(err);
            })
    };

    //listener for reason textfield
    const onChangeReason = (e) => {
        setReason(e.target.value);
    };

    return (
        <div>
            <Dialog
                open={open}
                TransitionComponent={Transition}
                keepMounted
                fullWidth={true}
                maxWidth={'sm'}
                onClose={handleClose}
                aria-labelledby="alert-dialog-slide-title"
                aria-describedby="alert-dialog-slide-description"
            >
                <DialogTitle>Reason</DialogTitle>
                <DialogContent>
                    <TextField
                        autoFocus
                        margin="dense"
                        name="reason"
                        label="Reason"
                        type="text"
                        fullWidth
                        onChange={onChangeReason}
                        value={reason}
                    />
                    {
                        hourToBook !== null &&
                        <small className="slot-label">
                            Selected slot: {moment.utc(hourToBook*3600*1000).format('h:mm a')}
                        </small>}
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="secondary">
                        Cancel
                    </Button>
                    <Button onClick={onClickBook} className="btn-dark" disabled={isLoading}>
                        {
                            isLoading ? <CircularProgress size={20} /> : 'Book'
                        }
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}
