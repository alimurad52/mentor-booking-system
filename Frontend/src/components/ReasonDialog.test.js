import React from "react";
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });
import ReasonDialog from "./ReasonDialog";

describe("Dialog component", () => {
    it("Should render dialog box", () => {
        const wrapper = shallow(
            <ReasonDialog
                open={true}
                hourToBook={20}
                date={new Date()}
                handleClose={() => void 0}
                bookSlot={() => void 0}
                isLoading={false}
            />);
    });
});
