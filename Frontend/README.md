### [Demo](https://careerfoundry.ali-murad.com/)

# Getting Started

- Run `git clone https://bitbucket.org/alimurad52/mentor-booking-system/src/master/`
- `cd Frontend`
- Run `npm install`
- Run `npm start`

# Test Suites
To run test case: `npm run test`

Available test suites:  

- Hours component   
    - Contains one test case checking the number of times the hours component is rendered    
- Reason dialog component   
    - Checks if dialog box is rendered   
- Sidebar component   
    - Checks if hours function returns expected hour object   
    - Checks if sidebar renders   
- Topbar component   
    - Checks if topbar is rendered   

# Folder structure

- Main file `-index.js`  
- App container file `-App.js`  
- Global styles `-_styles.scss`  
- Global/reusable variables `-const.js`
- Components folder `--components`
    - Hours component for sidebar and test file `---Hours.js  ---Hours.test.js`
    - Confirmation and reason dialog box and test file `---ReasonDialog.js ---ReasonDialog.test.js`
    - Drawer for slots and test file `---Sidebar.js ---Sidebar.test.js`
    - App bar and test file `---Topbar.js ---Topbar.test.js`
    
# Libraries used
    
- For date handling `moment.js`
- Layout library `material-ui`
- To build sass files `node-sass`
- Calendar view `react-calendar`
- To make requests `axios`

# How to book a slot?

1. Select date
2. Select available slot
3. Key in the reason and click `Book`

# Why this design?

Material design requires minimal css and has a very clean and impressive set of components, which are all responsive and mobile friendly. The idea of this layout is to keep it easy for the user to understand and keep the layout as minimal as possible.




